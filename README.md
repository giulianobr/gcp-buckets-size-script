# gcp-buckets-size-script



## Getting started

Simple script to create a CVS file in a bucket with all your buckets ize information.

the CSV format is: project_name,bucket_name,region,storage_class,size_bytes

    !! IT CAN TAKE UP TO 10 minutes TO FINISH !!

## Pip

```bash
pip install -r requirements.txt
```

## GCP Auth

With you gcloud sdk installed and configured you need to setup the application credential:

```bash
gcloud auth application-default login
```

## Run

First change the project name and the name name inside the main.py file.

```bash
python main.py
```
