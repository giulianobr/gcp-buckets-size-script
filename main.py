import os
import time

import google
from google.auth.transport.requests import AuthorizedSession
from google.cloud import monitoring_v3, storage

project_name = os.getenv('PROJECT', 'projects/YOUR_PROJECT_NAME_WITH_CLOUD_MONITORING_ACTIVATED')
bucket_name = os.getenv('BUCKET', 'YOUR_BUCKET_TO_UPLOAD_THE_CSV')

start_time = time.time()


def get_bucket_info(bckt_nm):
    google_api_url = f"https://storage.googleapis.com/storage/v1/b/{bckt_nm}"
    credentials, project = google.auth.default()
    authed_session = AuthorizedSession(credentials)

    response = authed_session.get(google_api_url)
    if response.ok:
        data = response.json()
        retentionPeriod = 'None'
        versioning = False
        if data.get('retentionPolicy'):
            retentionPeriod = data['retentionPolicy']['retentionPeriod']
        if data.get('versioning'):
            versioning = data['versioning']['enabled']
        return f"{data['updated']},{versioning},{retentionPeriod}"


def print_bucket_sizes():
    client = monitoring_v3.QueryServiceClient()

    request = monitoring_v3.QueryTimeSeriesRequest(
        name=project_name,
        query="""fetch gcs_bucket | metric 'storage.googleapis.com/storage/total_bytes' | 
        group_by 1h, [value_total_bytes_max: max(value.total_bytes)] |
        every 1h""",
    )
    results = client.query_time_series(request=request)

    csv = 'project_name,bucket_name,region,storage_class,size_bytes,last_updated,versioning,retention\n'
    nm_buckets = 0
    for result in results:
        project_nm = result.label_values[0].string_value
        buck_nm = result.label_values[1].string_value
        b1 = result.label_values[2].string_value
        b2 = result.label_values[3].string_value
        buck_sz = result.point_data[0].values[0].double_value
        csv += f'{project_nm},{buck_nm},{b1},{b2},{buck_sz}'
        csv += f',{get_bucket_info(bckt_nm=buck_nm)}\n'
        nm_buckets += 1

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob('bucket_size_projects.csv')
    blob.upload_from_string(csv)
    print(f'File uploaded. \nNumber of buckets in the CSV: {nm_buckets}')
    print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == '__main__':
    print_bucket_sizes()
